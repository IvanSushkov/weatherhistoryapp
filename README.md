# WeatherHistoryApp

## Требования

Операционной ситемой должна быть одна из Unix-подобных операционных систем. В системе должен быть установлен `docker` и `docker-compose`, утилита `unzip`, `Python3` и python пакет `scrapy-do`. 
Scrapy-do можно установить используя пакетный менеджер pip: `pip install scrapy-do`.

## Развертывание проекта

 * В корне проекта выполнить команду: `docker-compose up -d --build`.
 * В корне проекта выполнить команду: `docker exec -it weatherhistoryapp_web bash` для входа в контейнер веб приложения.
 * Выполнить команду `python manage.py migrate` для приминения миграций.
 * Создать аккаунт администратора `python manage.py createsuperuser` и заполнив запрашиваемые данные, после этого 
 по адресу http://127.0.0.1:8080/admin можно войти административную часть.
 * Собрать статические файлы командой `python manage.py collectstatic`.
 * Выйти из докер контейнера, нажав `Ctrl+D`
 * В корне проекта выполнить команды: 
 `scrapy-do-cl push-project --project-path crawler/` и
 `scrapy-do-cl schedule-job --project weather --spider openweathermap --when 'every 5 minutes'`

## REST Api
Сервис предоставляет api для получения текущей погоды, в качестве параметра `{city}` предается название города, регистр
не учитывается.

```
[GET]   http://127.0.0.1:8080/weather/current/?q={city}
```

## Scrapy Do

После развертывания по адресу http://127.0.0.1:7654/ доступна веб панель отображающая состояние парсеров, она может не
подгрузиться для этого нужно нажать `Ctrl+Shift+R`.