from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from .models import Weather


class SaveCrawledWeatherItemPipeline(object):
    def __init__(self, connection_uri):
        self.engine = create_engine(connection_uri)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            connection_uri=crawler.settings.get('DATABASE_DSN'),
        )

    def open_spider(self, spider):
        self.session = Session(bind=self.engine)

    def close_spider(self, spider):
        self.session.commit()
        self.session.close()

    def process_item(self, item, spider):
        weather = Weather(
            city=item['city'],
            temp=item['temp'],
            source=item['source'],
        )
        self.session.add(weather)

        return item
