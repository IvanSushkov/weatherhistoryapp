import scrapy


class WeatherItem(scrapy.Item):
    city = scrapy.Field()
    temp = scrapy.Field()
    source = scrapy.Field()
