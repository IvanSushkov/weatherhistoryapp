import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Float, DateTime, String, Integer

BaseModel = declarative_base()


class Weather(BaseModel):
    __tablename__ = 'weather_weather'

    id = Column(Integer, primary_key=True)
    city = Column(String)
    temp = Column(Float)
    source = Column(String)
    timestamp = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, city, temp, source):
        self.city = city
        self.temp = temp
        self.source = source
