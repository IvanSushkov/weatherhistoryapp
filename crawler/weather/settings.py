BOT_NAME = 'weather'

SPIDER_MODULES = ['weather.spiders']
NEWSPIDER_MODULE = 'weather.spiders'


ITEM_PIPELINES = {
    'weather.pipelines.SaveCrawledWeatherItemPipeline': 100,
}

DATABASE_DSN = 'postgresql://postgres@db/postgres'
