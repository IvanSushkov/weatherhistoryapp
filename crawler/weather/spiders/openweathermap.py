import json
import scrapy

from .cities import CITIES
from ..items import WeatherItem


class OpenweathermapSpider(scrapy.Spider):
    api_key = '35f2a70c335b73b764a832ffe9cd1648'
    name = 'openweathermap'
    allowed_domains = ['openweathermap.org']

    def start_requests(self):
        api_url = 'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={api_key}&units=metric'
        for city in CITIES:
            yield scrapy.Request(api_url.format(city_name=city, api_key=self.api_key), cb_kwargs={'city': city})

    def parse(self, response, **kwargs):
        json_data = json.loads(response.body_as_unicode())

        weather = WeatherItem(
            city=kwargs['city'],
            temp=json_data['main']['temp'],
            source=self.name,
        )

        yield weather
