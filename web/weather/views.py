from django.http.response import JsonResponse

from .models import Weather


def get_current_weather(request):
    city = request.GET.get('q', '').strip()

    if not city:
        return JsonResponse({'status': 'failed', 'msg': 'You must set `q` parameter value'},  status=400)

    try:
        weather = Weather.objects.filter(city__iexact=city).latest('timestamp')
    except Weather.DoesNotExist:
        return JsonResponse({'status': 'failed', 'msg': 'City not found'}, status=404)

    return JsonResponse({'status': 'ok', 'weather': weather.to_json()})
