from django.db import models


class Weather(models.Model):
    city = models.CharField(max_length=150, db_index=True)
    temp = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    source = models.CharField(max_length=150)

    def to_json(self):
        return {
            'temp': self.temp,
            'timestamp': self.timestamp,
        }

    def __str__(self):
        return self.city
