from django.contrib import admin

from .models import Weather


@admin.register(Weather)
class WeatherAdmin(admin.ModelAdmin):
    ordering = ['-timestamp']
    list_filter = ['city', 'timestamp', 'source']
    list_display = ['city', 'temp', 'timestamp', 'source']
