from django.urls import path

from .views import get_current_weather

app_name = 'weather'

urlpatterns = [
    path('current/', get_current_weather, name='current')
]
